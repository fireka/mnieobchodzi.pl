## Starter pack

Front end build for html/css/js

## Installation

* install all [bower](http://bower.io/) components

```bash
	bower install
```

* install npm packages including [Grunt](http://gruntjs.com/)

```bash
	npm install
```

* run 'grunt init'


* Grunt tasks

```bash
	grunt  
```

default development task

```bash
	grunt init
```
initializes project by copying bootstrap and other components files


```bash
	grunt prod
```

## Default settings include

* jquery.js
* bootstrap
