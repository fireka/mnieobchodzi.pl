'use strict';
var LIVERELOAD_PORT = 35729;
var lrSnippet = require('connect-livereload')({port: LIVERELOAD_PORT});
var componentsDir = 'bower_components';
var mountFolder = function (connect, dir) {
    return connect.static(require('path').resolve(dir));
};

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {
    // load all grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.initConfig({

        watch: {
            options: {
                nospawn: true
            },
            sass: {
                files: [
                  'css/components/**',
                  'css/main.scss'
                ],
                tasks: ['sass']
            },
            livereload: {
                options: {
                    livereload: LIVERELOAD_PORT
                },
                files: [
                  '*.html',
                  'css/{,*/}*.css',
                  'js/{,*/}*.js',
                  'img/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
                ]
            }
        },

        connect: {
            options: {
                port: 9000,
                // change this to '0.0.0.0' to access the server from outside
                hostname: 'localhost'
            },
            livereload: {
                options: { livereload: true },
            }
        },

        open: {
            server: {
                path: 'http://localhost:<%= connect.options.port %>'
            }
        },

        sass: {
          options: {
          	  outputStyle: 'compressed',
              sourceMap: false
            },
            dist: {
              files: {
                'css/style.css': 'css/style.scss'
            }
          }
        },

        copy: {
            bs3: {
                files: [
                  {
                    expand: true,
                    //flatten: true,
                    cwd: componentsDir+'/bootstrap-sass/assets/stylesheets/bootstrap',
                    src: '**',
                    //src: [ componentsDir+'/bootstrap-sass/assets/stylesheets/bootstrap/**' ],
                    dest: 'css/components/bootstrap/bootstrap',
                    filter: 'isFile'

                  }
                ],
            },
            bs3base: {
                files: [
                  {
                    expand: true,
                    flatten: true,
                    src: [ componentsDir+'/bootstrap-sass/assets/stylesheets/_bootstrap.scss' ],
                    dest: 'css/components/bootstrap',
                    filter: 'isFile'
                  }
                ],
            },
            animate: {
                files: [
                  {
                    expand: true,
                    flatten: true,
                    src: [componentsDir+'/animate.css/animate.css'],
                    dest: 'css/components/',
                    filter: 'isFile',
                    rename: function(dest, src) {
                      return dest + src.replace(/\.css$/, ".scss");
                    }
                  }
                ],
            }
        },
        concat: {
            js: {
              files: {
                'js/scripts.js': [
                   componentsDir+'/jquery/dist/jquery.js',
                   componentsDir+'/bootstrap-sass/assets/javascripts/bootstrap.js',
                   componentsDir+'/velocity/velocity.js',
                  //'js/typed.min.js',
                  'js/main.js'
                ],
              }
            }
        },

        autoprefixer: {
          options: {
            browsers: ['last 8 versions']
          },
          no_dest: {
            src: 'css/style.css'
          },
        },

        uglify: {
          dist: {
	          options: {
	          mangle: true
	          },
	          files: {
	            'js/scripts.min.js': ['js/scripts.js'],
	          }
          }

        },

        uncss: {
          dist: {
          	options: {
          	  ignore: ['.modal', '.fade', '.modal-dialog', '.modal-content', '.modal-header', '.info--modall', '.text-center', '.in']
      		},
            files: {
              'css/style.css': ['index.html']
            }
          }
        },

        remove: {
            default_options: {
              trace: true,
              dirList: ['css/build/', 'scripts/']
            }
        },

    });



    grunt.registerTask('init', function (target) {
        grunt.task.run([
            'copy'
        ]);
    });


    grunt.registerTask('default', function (target) {
        grunt.task.run([
            'sass',
            'concat:js',
            'uglify',
            'autoprefixer',
           // 'uncss',
            'watch'
        ]);
    });

    grunt.registerTask('prod', function (target) {
        grunt.task.run([
            'sass',
            'concat:js',
            'uglify',
            'autoprefixer',
            //'uncss',
        ]);
    });

};
