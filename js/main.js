 $(function(){

 	$('section').hide();
 	$('#intro, #start').show();

 	var startHtml = $('#start').html();

 	$('.start--btn').click(function(e) {
 		e.preventDefault();
 		$('html, body').animate({
 			scrollTop:$('#start').offset().top
 		}, 500);
 	});


 	$(document).on('click', '.hero-link, .option--box, .btn--back, .btn--start, .btn--last, .btn--nav', function(e) {
 		e.preventDefault();
 		var target = $(this).attr('href');
 		if($(this).hasClass('btn--nav')) {
	  		if(target == '#onas') {
	 			//swap nav link and text
	 			$(this).text('Home').attr('href','#start');
	 		} else {
	 			$(this).text('O nas').attr('href','#onas');

	 		}
 		}

 		animateSections($(target));

 	});



 	function animateSections(targetEl) {
 		var startEl = $('#start');
 		startEl.find('.hero-text').velocity({
 			opacity:0,
 		},{
 			duration:400,
 			complete: function() {
 				$('html, body').animate({
 					scrollTop:startEl.offset().top
 				}, 500);

 				var html = ''
 				if(targetEl.attr('id') == 'start') {
 					html = startHtml;
 				} else {
 					html = targetEl.find('.hero-text').html();
 				}
 				startEl.find('.hero-text').html(html).velocity({
 					opacity:1
 				},400)

 			}
 		})
 	}
 });

function fbShare(url, title, descr, winWidth, winHeight) {
    var winTop = (screen.height / 2) - (winHeight / 2);
    var winLeft = (screen.width / 2) - (winWidth / 2);
    window.open('http://www.facebook.com/sharer.php?s=100&p[title]=' + title + '&p[summary]=' + descr + '&p[url]=' + url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
}

